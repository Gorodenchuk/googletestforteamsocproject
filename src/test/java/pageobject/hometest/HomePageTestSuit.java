package pageobject.hometest;

import org.testng.Assert;
import org.testng.annotations.Test;

import pageObject.pages.enterapplication.AccountsPage;
import pageObject.pages.enterapplication.EmailLoginPage;
import pageObject.pages.enterapplication.HomeGPage;
import pageObject.pages.enterapplication.PasswordLoginPage;
import pageobject.testcase.TestBase;

public class HomePageTestSuit extends TestBase {
	@Test
	// TS 1.1
	public void logOutV1() throws InterruptedException {
		EmailLoginPage emailLoginPage = homeGPage.clickOnEmailButton();
		PasswordLoginPage passwordLoginPage = emailLoginPage.enterEmailV1();
		HomeGPage homeGPage = passwordLoginPage.enterPasswordV1();
		AccountsPage accountsPage = homeGPage.logOut();
		homeGPage = accountsPage.clickOnSignOutButton();
		Assert.assertTrue(homeGPage.isTextDisplayed(), "Word is not displayed");
	}

	@Test
	// TS 1.2
	public void logOutV2() throws InterruptedException {
		EmailLoginPage emailLoginPage = homeGPage.clickOnEmailButton();
		PasswordLoginPage passwordLoginPage = emailLoginPage.enterEmailV2();
		HomeGPage homeGPage = passwordLoginPage.enterPasswordV2();
		AccountsPage accountsPage = homeGPage.logOut();
		homeGPage = accountsPage.clickOnSignOutButton();
		Assert.assertTrue(homeGPage.isTextDisplayed(), "Word is not displayed");
	}

	@Test
	// TS 1.3
	public void logOutV3() throws InterruptedException {
		EmailLoginPage emailLoginPage = homeGPage.clickOnEmailButton();
		PasswordLoginPage passwordLoginPage = emailLoginPage.enterEmailV3();
		HomeGPage homeGPage = passwordLoginPage.enterPasswordV3();
		AccountsPage accountsPage = homeGPage.logOut();
		homeGPage = accountsPage.clickOnSignOutButton();
		Assert.assertTrue(homeGPage.isTextDisplayed(), "Word is not displayed");
	}

	@Test
	// TS 1.4
	public void logOutV4() throws InterruptedException {
		EmailLoginPage emailLoginPage = homeGPage.clickOnEmailButton();
		PasswordLoginPage passwordLoginPage = emailLoginPage.enterEmailV4();
		HomeGPage homeGPage = passwordLoginPage.enterPasswordV4();
		AccountsPage accountsPage = homeGPage.logOut();
		homeGPage = accountsPage.clickOnSignOutButton();
		Assert.assertTrue(homeGPage.isTextDisplayed(), "Word is not displayed");
	}

	@Test
	// TS 1.5
	public void logOutV5() throws InterruptedException {
		EmailLoginPage emailLoginPage = homeGPage.clickOnEmailButton();
		PasswordLoginPage passwordLoginPage = emailLoginPage.enterEmailV5();
		HomeGPage homeGPage = passwordLoginPage.enterPasswordV5();
		AccountsPage accountsPage = homeGPage.logOut();
		homeGPage = accountsPage.clickOnSignOutButton();
		Assert.assertTrue(homeGPage.isTextDisplayed(), "Word is not displayed");
	}

	@Test
	// TS 1.6
	public void logOutV6() throws InterruptedException {
		EmailLoginPage emailLoginPage = homeGPage.clickOnEmailButton();
		PasswordLoginPage passwordLoginPage = emailLoginPage.enterEmailV6();
		HomeGPage homeGPage = passwordLoginPage.enterPasswordV6();
		AccountsPage accountsPage = homeGPage.logOut();
		homeGPage = accountsPage.clickOnSignOutButton();
		Assert.assertTrue(homeGPage.isTextDisplayed(), "Word is not displayed");
	}

}
