package pageobject.testcase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import pageObject.pages.enterapplication.HomeGPage;
import pageObject.pages.webDriver.WebDriverFactory;

//import  pageObject.pages.webDriver.WebDriverFactory;

/*
 * Base class for all the test classes
 */

public class TestBase {

	protected WebDriver webDriver;
	protected pageObject.pages.enterapplication.HomeGPage homeGPage;

	@BeforeMethod
	@Parameters({ "browserName" })
	public void setup(String browserName) throws Exception {
		webDriver = WebDriverFactory.getInstance(browserName);
		webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		// webDriver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		webDriver.manage().window().maximize();
		webDriver.get("https://www.google.com.ua/");
		homeGPage = PageFactory.initElements(webDriver, HomeGPage.class);
	}

	@AfterMethod
	public void tearDown() throws Exception {
		if (webDriver != null) {
			WebDriverFactory.killDriverInstance();
		}
	}

}