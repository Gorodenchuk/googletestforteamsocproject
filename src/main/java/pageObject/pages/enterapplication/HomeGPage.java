package pageObject.pages.enterapplication;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import pageObject.pages.Page;

public class HomeGPage extends Page {

	@FindBy(how = How.XPATH, using = "//*[@id='gb_70']")
	private WebElement enterButton;

	@FindBy(how = How.XPATH, using = ".//*[@id='gbw']/div/div/div[2]/div[4]/div[1]/a/span")
	private WebElement circleAccountButton;

	@FindBy(how = How.XPATH, using = ".//*[@id='gb_71']")
	private WebElement LogOutButton;

	@FindBy(how = How.XPATH, using = "//*[.='Увійти']")
	private WebElement TextOnExitButton;

	public HomeGPage(WebDriver webDriver) {
		super(webDriver);
		// TODO Auto-generated constructor stub
	}

	public EmailLoginPage clickOnEmailButton() {
		enterButton.click();
		return PageFactory.initElements(webDriver, EmailLoginPage.class);
	}

	public AccountsPage logOut() throws InterruptedException {
		circleAccountButton.click();
		// LogOutButton.click();
		return PageFactory.initElements(webDriver, AccountsPage.class);
	}

	public boolean isTextDisplayed() {
		try {
			return TextOnExitButton.isDisplayed();
		} catch (Exception e) {
			return false;
		}

	}

}
