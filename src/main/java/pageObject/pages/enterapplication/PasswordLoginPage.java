package pageObject.pages.enterapplication;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import pageObject.pages.Page;
import workingWithFiles.PropertyLoader;

public class PasswordLoginPage extends Page {

	@FindBy(how = How.XPATH, using = "//*[@id='Passwd']")
	private WebElement passwordField;

	public PasswordLoginPage(WebDriver webDriver) {
		super(webDriver);
		// TODO Auto-generated constructor stub
	}

	public HomeGPage enterPasswordV1() {
		passwordField.clear();
		passwordField.sendKeys(PropertyLoader.loadProperty("v1_correct.password"));
		passwordField.submit();
		return PageFactory.initElements(webDriver, HomeGPage.class);
	}

	public HomeGPage enterPasswordV2() {
		passwordField.clear();
		passwordField.sendKeys(PropertyLoader.loadProperty("v2_correct.password"));
		passwordField.submit();
		return PageFactory.initElements(webDriver, HomeGPage.class);
	}

	public HomeGPage enterPasswordV3() {
		passwordField.clear();
		passwordField.sendKeys(PropertyLoader.loadProperty("v3_uncorrect.password"));
		passwordField.submit();
		return PageFactory.initElements(webDriver, HomeGPage.class);
	}

	public HomeGPage enterPasswordV4() {
		passwordField.clear();
		passwordField.sendKeys(PropertyLoader.loadProperty("v4_uncorrect.password"));
		passwordField.submit();
		return PageFactory.initElements(webDriver, HomeGPage.class);
	}

	public HomeGPage enterPasswordV5() {
		passwordField.clear();
		passwordField.sendKeys(PropertyLoader.loadProperty("v5_uncorrect.password"));
		passwordField.submit();
		return PageFactory.initElements(webDriver, HomeGPage.class);
	}

	public HomeGPage enterPasswordV6() {
		passwordField.clear();
		passwordField.sendKeys(PropertyLoader.loadProperty("v6_correct.password"));
		passwordField.submit();
		return PageFactory.initElements(webDriver, HomeGPage.class);
	}
}