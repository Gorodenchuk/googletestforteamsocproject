package pageObject.pages.enterapplication;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import pageObject.pages.Page;
import workingWithFiles.PropertyLoader;

public class EmailLoginPage extends Page {

	@FindBy(how = How.XPATH, using = "//*[@id='Email']")
	private WebElement mailField;

	public EmailLoginPage(WebDriver webDriver) {
		super(webDriver);
		// TODO Auto-generated constructor stub
	}

	public PasswordLoginPage enterEmailV1() {
		mailField.clear();
		mailField.sendKeys(PropertyLoader.loadProperty("v1_correct.username"));
		mailField.submit();
		return PageFactory.initElements(webDriver, PasswordLoginPage.class);
	}

	public PasswordLoginPage enterEmailV2() {
		mailField.clear();
		mailField.sendKeys(PropertyLoader.loadProperty("v2_uncorrect.username"));
		mailField.submit();
		return PageFactory.initElements(webDriver, PasswordLoginPage.class);
	}

	public PasswordLoginPage enterEmailV3() {
		mailField.clear();
		mailField.sendKeys(PropertyLoader.loadProperty("v3_correct.username"));
		mailField.submit();
		return PageFactory.initElements(webDriver, PasswordLoginPage.class);
	}

	public PasswordLoginPage enterEmailV4() {
		mailField.clear();
		mailField.sendKeys(PropertyLoader.loadProperty("v4_uncorrect.username"));
		mailField.submit();
		return PageFactory.initElements(webDriver, PasswordLoginPage.class);
	}

	public PasswordLoginPage enterEmailV5() {
		mailField.clear();
		mailField.sendKeys(PropertyLoader.loadProperty("v5_correct.username"));
		mailField.submit();
		return PageFactory.initElements(webDriver, PasswordLoginPage.class);
	}

	public PasswordLoginPage enterEmailV6() {
		mailField.clear();
		mailField.sendKeys(PropertyLoader.loadProperty("v6_uncorrect.username"));
		mailField.submit();
		return PageFactory.initElements(webDriver, PasswordLoginPage.class);
	}

}
