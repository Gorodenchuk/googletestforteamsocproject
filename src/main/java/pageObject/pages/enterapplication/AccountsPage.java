package pageObject.pages.enterapplication;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import pageObject.pages.Page;

public class AccountsPage extends Page {

	@FindBy(how = How.XPATH, using = "//*[@id='signout']")
	private WebElement SignOutButton;

	public AccountsPage(WebDriver webDriver) {
		super(webDriver);
		// TODO Auto-generated constructor stub
	}

	public HomeGPage clickOnSignOutButton() {
		SignOutButton.click();
		return PageFactory.initElements(webDriver, HomeGPage.class);
	}

}
